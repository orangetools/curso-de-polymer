Es importante que al bindear las propiedades, no tenga espacios entre las llaves y la etiqueta: 
El problema no esta en las cookies. 
De acuerdo a la documentación: 
String concatenation is not supported inside a tag, and the tag can’t contain any whitespace

#Preguntas de la clase 7:
* Cual es la función de Properties que se usa en la función Polymer dentro del scrip del modulo, si al quitarla sigue funcionando sin problemas??

Properties nos permite configurar el elemento, type nos indica el tipo de data con la que se va a trabajar, en este caso una simple cadena de texto, en cuanto a value, este indica el valor por defecto que tomara el elemento si no se le asigna un valor, en este caso who sera igual a 'Platzi'.

Le da un valor default. Si en el que consume el componente no le pone el atributo "who=..." va a tomar el valor que se puso el la properties.

* Polymer con React, se puede usar? 

Polymer y react son comparables pero no del todo. Cuando creamos WebComponents con Polymer, nuestro Framework es el mismo DOM, cuando en React, se tiene un DOM propio. Se pueden utilizar juntos, pero no siempre es recomendable. 

* Polymer y SEO. 

Polymer es excelente para SEO por los tags, pero depende mucho del desarrollador. 
No depende tanto de los frameworks utilizado. 

* Beneficios de Polymer: 

Legibilidad y reutilizabilidad. 
Para esto es necesario que el código sea Modular. Este es el mejor beneficio de Polymer. 
No depende del Backend ya que es una librería de FrontEnd. 

# EXTERNAL TOOLS
## Yeoman

[Yeoman](http://yeoman.io/)
Yeoman helps you to kickstart new projects, prescribing best practices and tools to help you stay productive. To do so, we provide a generator ecosystem. A generator is basically a plugin that can be run with the `yo` command to scaffold complete projects or useful parts.

### Install

1. npm install -g yo

## Polymer CLI

[Polymer CLI](https://github.com/Polymer/tools/tree/master/packages/cli)
The command-line tool for Polymer projects and Web Components.

### Install

1. npm install -g polymer-cli
2. polymer init

### Features

* init - Create a new Polymer project from pre-configured starter templates
* install - Install dependencies and dependency variants via Bower
* serve	- Serve elements and applications during development
* lint - Lint a project to find and diagnose errors quickly
* test - Test your project with web-component-tester
* build	- Build an application optimized for production
* analyze - Generate an analyzed JSON representation of your element or application